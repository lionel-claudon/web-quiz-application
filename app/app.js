(function() {
    'use strict';

    var quizApp = angular.module('quizApp', [
        'ngRoute',
        'ui.bootstrap'
    ]).config(function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : 'pages/welcome.html',
                controller  : 'mainQuizController'
            })

            // route for the quiz page
            .when('/quiz', {
                templateUrl : 'pages/quiz.html',
                controller  : 'quizController'
            })

            // route for the result page
            .when('/result', {
                templateUrl : 'pages/result.html',
                controller  : 'quizResultController'
            });
    });

    quizApp.controller('mainQuizController', ['$scope', '$injector' ,'$http', '$window' , '$location', function($scope, $injector, $http, $window, $location) {
        var $rootScope = $injector.get('$rootScope');

        $http.get('data/questions.json').then(function (response) {
            $scope.questions = response.data;

            if ($scope.questions.length > 0) {
                var options = [
                    {id: '1', name: '5'},
                    {id: '2', name: '20'},
                    {id: '3', name: '30'},
                    {id: '4', name: '40'},
                    {id: '5', name: $scope.questions.length}
                ];

                $scope.quizQuestionsNumber = {
                    availableOptions: options,
                    selectedOption: options[3]
                };

                $scope.startQuizDisabled = false;
            }
        }).catch(function () {
            // start http server for this to work
            $scope.startQuizDisabled = true;
            $window.alert("Couldn't load the questions from server or file system");
        });

        $scope.startQuiz = function() {
            var questionsIds = [];
            var quizQuestions = [];
            var number =  Number($scope.quizQuestionsNumber.selectedOption.name);

            while(quizQuestions.length < number) {
                var index = Math.round(Math.random() * $scope.questions.length);
                if(questionsIds.indexOf(index) == -1) {
                    questionsIds.push(index);
                    quizQuestions.push($scope.questions[index]);
                }
            }

            $rootScope.questions = quizQuestions;

            $location.path('/quiz');
        };

    }
    ]);

    quizApp.controller('quizController', ['$scope', '$injector', '$location',
        function($scope, $injector, $location) {
            var $rootScope = $injector.get('$rootScope');

            console.log("Welcome in the quiz!");
            $scope.Answer = function(text) {
                this.text = text;
                this.isSelected = false;
                this.class = '';
            };

            var selectedOptionsCount = 0;
            $scope.userScore = 0;
            $scope.countFrom = 0;
            $scope.countTo = $scope.questions.length;
            var currentQuestion;


            $scope.optionSelected = function(optionSelected) {
                optionSelected ? selectedOptionsCount++ : selectedOptionsCount--;
                if(selectedOptionsCount>0) {
                    $scope.validateAnswerDisabled = false;
                } else {
                    $scope.validateAnswerDisabled = true;
                }
            };

            $scope.goToNextQuestion = function() {
                // Detect if it's the end of the quiz
                if($scope.countFrom == $scope.countTo) {
                    $rootScope.userScore = Math.round(100*($scope.userScore / $scope.countTo));
                    $location.path('/result');
                    return;
                }

                selectedOptionsCount = 0;
                $scope.goToNextQuestionButtonVisible = false;
                $scope.validateAnswerDisabled = true;
                currentQuestion = $scope.questions[$scope.countFrom++];
                $scope.question = currentQuestion.question;
                $scope.currentAnswers = [];

                // Prepare answers model
                for(var i in currentQuestion.answers) {
                    $scope.currentAnswers.push(new $scope.Answer(currentQuestion.answers[i]));
                }
            };

            $scope.validateQuestion = function() {
                $scope.goToNextQuestionButtonVisible = true;

                var userScored = true;
                var validAnswersText = [];
                for(var v in currentQuestion.validAnswers) {
                    validAnswersText.push(currentQuestion.answers[currentQuestion.validAnswers[v]]);
                }


                for(var u in $scope.currentAnswers) {
                    var userAnswer = $scope.currentAnswers[u];
                    var userAnswerText = userAnswer.text;

                    if (userAnswer.isSelected && validAnswersText.indexOf(userAnswerText) > -1) {
                        userAnswer.class = 'quiz-valid-answer';
                    } else if (!userAnswer.isSelected && validAnswersText.indexOf(userAnswerText) > -1) {
                        userAnswer.class = 'quiz-valid-answer';
                        userScored = false;
                    } else if (userAnswer.isSelected && !(validAnswersText.indexOf(userAnswerText) > -1)) {
                        userAnswer.class = 'quiz-invalid-answer';
                        userScored = false;
                    }
                }
                if(userScored) $scope.userScore++;
            };

            $scope.goToNextQuestion();
        }
    ]);

    quizApp.controller('quizResultController', ['$scope', '$location', function($scope, $location) {
        $scope.goToMenu = function() {
            $location.path('/');
        };


        $scope.$on('$routeChangeStart', function (scope, next, current) {
            if (next.$$route.controller === "quizController") {
                $scope.goToMenu();
            }
        });
    }
    ]);


})();